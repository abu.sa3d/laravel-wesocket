# How to setup
1. ###cd to docker directory
    ``` bash
    cd docker
    ```
1. ### create your `.env` file by copy `.env.eample` file to a new file named `.env`
    ``` bash
    cp .env.example .env
    ```
    
3. ### Configure `.env` according to your needs

4. ### Build with default configurations
    you can build the project with its default configurations
    or you can take a look first at the next Configuration section
    ``` bash
    # build
    docker-compose up -d nginx mysql workspace php-fpm phpmyadmin
    
    # access workspace
    docker-compose exec workspace bash
    ```
    
    you have to set `Lumen` `.env` database configuration to be able to connect to database container as follow
     - `DB_HOST`, `DB_READ_HOST` and `DB_WRITE_HOST` values will be used database container name `mysql` or `mariadb`
     - `DB_PORT` will be equals to docker .env `MYSQL_PORT` or `MARIADB_PORT`
     - `DB_DATABASE` will be equals to docker .env `MYSQL_DATABASE` or `MARIADB_DATABASE`
     - `DB_USERNAME` will be equals to docker .env `MYSQL_USERNAME` or `MARIADB_USERNAME`
     - `DB_PASSWORD` will be equals to docker .env `MYSQL_PASSWORD` or `MARIADB_PASSWORD`
    
    Migrate your app database
    ``` bash
    # assume you are inside workspace container shell
    art reset
    ```
    
5. ### Configure Containers
    #### __`nginx`__
    by default nginx will listen for http on `localhost` domain
    to use custom virtual domain you can create a copy from `boostiny.conf.eaxample` and name it as `boostiny.conf`
    and at the line `server_name boostiny.test;` change `boostiny.test` to your desired virtual host or adding a new domains or subdomains separated by spaces
    
    to use `ssl` `https` you can also use `boostiny-ssl.conf.example` instead of `boostiny.conf.eaxample` 
    but you have to make sure that you have provided certificate files inside `ssl` directory
    
    > after any change you have to re build nginx container if was already built:
    ```bash
    # stop the container if already running
    docker-compose stop nginx
 
    # Re Build the container
    docker-compose build nginx
    ```
    #### __`mysql`__
    - by default a new database will be created according to the given mysql data in `.env` file
    when building the container for the first time
    
    - if the container is built before or you need to create another database
        1. you can activate `createdb.sql.example` by copying it without `.example` 
        2. write your own or custom sql commands in the file
        3. stop the container then rebuild
            ```bash
            docker-compose stop mysql
            docker-compose up -d --build mysql
            docker-compose exec mysql bash
            
            # inside the mysql container run the following command
            # enter root user password provided at .env MYSQL_ROOT_PASSWORD
            mysql -u root -p < /docker-entrypoint-initdb.d/createdb.sql
            ```
            
    #### __`phpmyadmin`__
    - you only have to adjust its `.env` configuration
    - `PMA_DB_ENGINE` will be `mysql` or `mariadb` according to which container you are running
    - to connect to phpmyadmin you will be asked to fill `server` field will be `mariadb` or `mysql` according to used container
    and user .env credentials `PMA_USER` && `PMA_PASSWORD` and `PMA_ROOT_PASSWORD`
    
    #### __`php-worker`__
    if you need to activate the workers:
    - activate workers configuration files inside `php-worker/supervisord.d` directory
    - rebuild the `php-worker` container if was already built
    
5. ### Start containers:

    to start basic containers workspace nginx mysql php-fpm
    ``` bash
    docker-compose up -d nginx mysql
    ```
    
    to start phpmyadmin container
    ``` bash
    docker-compose up -d phpmyadmin
    ```
    
    to start php-worker container
    ``` bash
    docker-compose up -d php-worker
    ```

6. ### Access containers:
    to access to project workspace to run artisan commands and composer commands
    ```bash
       docker-compose exec workspace bash
     ```
    and so on you can access any running container