## Laravel Websockets decorized app

### Start App
```bash
 # If its the first time to build docker we need .env file
 cp docker/.env.example docker/.env
 
 cd docker
 
 docker compose up -d
```

#### For the first time installation only
```bash
# Open workspace container
docker compose exec worspace bash

# .env file
cp .env.example .env

# run install
composer install

# migrate
art migrate:install #(if the migration table not exits)
art migrate

# exit container
exit 

# restart worker (to restart socket server)
docker compose stop php-worker
docker compose up -d php-worker
```

> ```
> app dashboard url: localhost:8008/dashboard
> ```

>```
> socket: localhost:6001

> Dashboard Login email: `geeks@boostiny.com`
> 
> Dashboard Password: `boostiny app password`

