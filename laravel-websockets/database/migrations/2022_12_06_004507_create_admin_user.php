<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = Carbon::now()->toDateTimeString();
        $admin = new User([
            'name' => 'Boostiny Admin',
            'email' => 'geeks@boostiny.com',
            'password' => Hash::make('#JLvxRx5j0P^'),
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        $admin->email_verified_at = $now;

        $admin->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
